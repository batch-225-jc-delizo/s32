// Node.js Routing w/ HTTP methods

let http = require("http");


const server = http.createServer(function(request, response) {

	// The http method of the incoming request can be accessed via the "method" property of the request parameter.
	if (request.url == "/items" && request.method == "GET"){

		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("Data retrieve from the database");
	}

	// The method "POST" means that we will be adding or creating information but for now, we will just be sending a text response for now.

	else if (request.url == "/items" && request.method == "POST"){

		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("Data to be sent to the database");
	}
}).listen(4000);

console.log('Server is running at localhost:4000')